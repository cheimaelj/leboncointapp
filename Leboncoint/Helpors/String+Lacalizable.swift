//
//  String+Lacalizable.swift
//  Leboncoint
//
//  Created by celj on 21/04/2023.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
