//
//  UIColor.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import Foundation
import UIKit

extension UIColor{
    static var backgroundColor : UIColor{
        UIColor(named: "BackgroundColor") ?? .white
    }
}
