//
//  Category.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import Foundation

struct CategoryProduct : Codable {
    let id : Int64?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int64.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
    init(){
        id = 0
        name  = ""
    }
    
    
    init(id : Int64?, name : String?){
        self.id = id
        self.name = name
    }

}
