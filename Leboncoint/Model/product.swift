//
//  Product.swift
//  Leboncoint
//
//  Created by celj on 14/04/2023.
//

import Foundation

struct Product : Codable {
    let id : Int64?
    let category_id : Int64?
    let title : String?
    let description : String?
    let price : Float?
    let images_url : Images_url?
    let creation_date : String?
    let is_urgent : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case category_id = "category_id"
        case title = "title"
        case description = "description"
        case price = "price"
        case images_url = "images_url"
        case creation_date = "creation_date"
        case is_urgent = "is_urgent"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int64.self, forKey: .id)
        category_id = try values.decodeIfPresent(Int64.self, forKey: .category_id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        price = try values.decodeIfPresent(Float.self, forKey: .price)
        images_url = try values.decodeIfPresent(Images_url.self, forKey: .images_url)
        creation_date = try values.decodeIfPresent(String.self, forKey: .creation_date)
        is_urgent = try values.decodeIfPresent(Bool.self, forKey: .is_urgent)
    }
    
    init(){
        id = 0
        category_id = 0
        title = ""
        description = ""
        price = 0
        images_url = Images_url.init()
        creation_date = ""
        is_urgent = false
    }
    
    init( id : Int64?, category_id : Int64?, title : String?, description : String?, price : Float?, images_url : Images_url?, creation_date : String?, is_urgent : Bool?){
        self.id = id
        self.category_id = category_id
        self.title = title
        self.description = description
        self.price = price
        self.images_url = images_url
        self.creation_date = creation_date
        self.is_urgent = is_urgent
        
    }
    
    
    
}
