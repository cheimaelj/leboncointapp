//
//  ProductCollectionViewCell.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .label
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let categoryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let productImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 8
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.textColor = .label
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let urgentLabel: UILabel = {
        let label = UILabel()
        label.text = "urgent".localized
        label.isHidden = true
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = .backgroundColor
        label.backgroundColor = .red
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Methods
    
    func configure(with product: Product, category: CategoryProduct){
        nameLabel.text = product.title
        priceLabel.text = String(product.price ?? 0) + "€"
        categoryLabel.text = category.name
        if product.is_urgent ?? false{
            urgentLabel.isHidden = false
        }else{
            urgentLabel.isHidden = true
        }
        
        self.productImageView.image = UIImage(named: "default-img")
        if let imageURL = URL(string: product.images_url?.thumb ?? ""){
            URLSession.shared.dataTask(with: imageURL) { data, response, error in
                if let data = data, let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self.productImageView.image = image
                    }
                }
            }.resume()
            
        }
        
    }
    
    // MARK: - Private Methods
    
    private func setupViews() {
        backgroundColor = .backgroundColor
       
        addSubview(productImageView)
        addSubview(nameLabel)
        addSubview(categoryLabel)
        addSubview(priceLabel)
        addSubview(urgentLabel)
      
        NSLayoutConstraint.activate([
            productImageView.topAnchor.constraint(equalTo: topAnchor),
            productImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            productImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            productImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.6),
            
            nameLabel.topAnchor.constraint(equalTo: productImageView.bottomAnchor, constant: 4),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            
            categoryLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
            categoryLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            categoryLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            
            priceLabel.topAnchor.constraint(equalTo: categoryLabel.bottomAnchor, constant: 4),
            priceLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            
            urgentLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 4),
            urgentLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            urgentLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            urgentLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            
        ])
    }
    
}

