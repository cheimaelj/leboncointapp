//
//  ImageViewModel.swift
//  Leboncoint
//
//  Created by celj on 23/04/2023.
//

import Foundation

class ImageViewModel {
    
    var coordinator: ImageViewCoordinator?
    let image: String
    
    init(image: String) {
        self.image = image
    }
}
