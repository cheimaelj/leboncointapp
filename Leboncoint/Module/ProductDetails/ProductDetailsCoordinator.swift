//
//  ProductDetailsCoordinator.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import Foundation
import UIKit

class ProductDetailsCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var parentCoordinator: Coordinator?
    private let product: Product
    private let category: CategoryProduct
    private var navigationController : UINavigationController
        
    init(navigationController: UINavigationController , product: Product, category : CategoryProduct) {
        self.navigationController = navigationController
        self.product = product
        self.category = category
    }
    
    func start() {
        let viewController = ProductDetailsViewController()
        let viewModel = ProductDetailsViewModel(product: product, category : category)
        viewController.viewModel = viewModel
        viewController.viewModel?.coordinator = self
        
        self.navigationController.pushViewController(viewController, animated: true)
        
    }
    
    func showImageView(image: String) {
        let imageViewCoordinator = ImageViewCoordinator(navigationController: navigationController, image: image)
        imageViewCoordinator.parentCoordinator = self
        imageViewCoordinator.start()
        childCoordinators.append(imageViewCoordinator)
    }
    
    func productDetailsDidFinish() {
        parentCoordinator?.childDidFinish(childCoordinator: self)
    }
}
