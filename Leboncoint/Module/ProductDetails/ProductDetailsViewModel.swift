//
//  ProductDetailsViewModel.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import Foundation

class ProductDetailsViewModel {
    
    var coordinator: ProductDetailsCoordinator?
    let product: Product
    let category: CategoryProduct
    
    init(product: Product , category: CategoryProduct) {
        self.product = product
        self.category = category
    }
    
    func showImage(){
        coordinator?.showImageView(image: product.images_url?.small ?? "")
    }
}
