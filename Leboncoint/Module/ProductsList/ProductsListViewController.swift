//
//  ProductsListViewController.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import UIKit

class ProductsListViewController: UIViewController{
    
    
    // MARK: - Properties
    
    private let productsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 12, bottom: 10, right: 12)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .backgroundColor
        collectionView.showsVerticalScrollIndicator = false
        return collectionView
    }()
    
    private let categoryCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 15, left: 12, bottom: 5, right: 12)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .backgroundColor
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    var viewModel: ProductsListViewModel?
    let activityIndicator = UIActivityIndicatorView(style: .large)
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationItem()
        setupCollectionView()
        setupViewModel()
        
    }
    
    // MARK: - Private Methods
    
    private func setupCollectionView() {
        var collectionViewHeightConstraint: NSLayoutConstraint!
        
        view.addSubview(categoryCollectionView)
        view.addSubview(productsCollectionView)
        
        productsCollectionView.delegate = self
        productsCollectionView.dataSource = self
        
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        
        productsCollectionView.register(ProductCollectionViewCell.self, forCellWithReuseIdentifier: "ProductCollectionViewCell")
        categoryCollectionView.register(CategoryCollectionViewCell.self, forCellWithReuseIdentifier: "CategoryCollectionViewCell")
        
        collectionViewHeightConstraint = categoryCollectionView.heightAnchor.constraint(equalToConstant: 80)
        collectionViewHeightConstraint.isActive = true
        
        NSLayoutConstraint.activate([
            
            categoryCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
            categoryCollectionView.leadingAnchor.constraint(equalTo:  view.leadingAnchor),
            categoryCollectionView.trailingAnchor.constraint(equalTo:  view.trailingAnchor),
            
            productsCollectionView.topAnchor.constraint(equalTo: categoryCollectionView.bottomAnchor),
            productsCollectionView.leadingAnchor.constraint(equalTo:  view.leadingAnchor),
            productsCollectionView.trailingAnchor.constraint(equalTo:  view.trailingAnchor),
            productsCollectionView.bottomAnchor.constraint(equalTo:  view.bottomAnchor, constant: -8)
            
        ])
        
    }
    
    private func setupNavigationItem(){
        let logoImage = UIImage(named: "Leboncoin_Logo")
        let logoImageView = UIImageView(image: logoImage)
        logoImageView.contentMode = .scaleAspectFit
        navigationItem.titleView = logoImageView
        navigationController?.setStatusBar(backgroundColor: .backgroundColor)
        view.backgroundColor = .backgroundColor
    }
    
    private func setupViewModel() {
        viewModel?.showLoader = { [weak self] shouldShow in
            shouldShow ? self?.activityIndicator.startAnimating() : self?.activityIndicator.stopAnimating()
        }
        viewModel?.refreshViewProducts = {
            self.productsCollectionView.reloadData()
        }
        viewModel?.refreshViewCategorys = {
            self.categoryCollectionView.reloadData()
        }
        viewModel?.showError = { [weak self]  error in
            self?.showError(error: error)
        }
        viewModel?.getProductsAndCategorys()
    }
    
    private func showError(error: String){
        let alertController = UIAlertController(title: error, message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ProductsListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == productsCollectionView {
            let collectionViewWidth = self.view.frame.width
            if UIDevice.current.userInterfaceIdiom == .phone {
                return CGSize(width: collectionViewWidth/2 - 20, height: collectionViewWidth * 5/6)
            }else{
                return CGSize(width: collectionViewWidth/3 - 20, height: collectionViewWidth/3 - 5)
            }
        }else{
            return CGSize(width: 130, height: 35)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

// MARK: - UICollectionViewDataSource

extension ProductsListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == productsCollectionView{
            return self.viewModel?.products.count ?? 0
        }else{
            return self.viewModel?.categorys.count ?? 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == productsCollectionView{
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
                return UICollectionViewCell()
            }
            let product =  viewModel?.getProducts(at: indexPath.row) ?? Product.init()
            let category = viewModel?.getCategorys(at: Int((product.category_id ?? 1)) - 1) ?? CategoryProduct.init()
            
            cell.configure(with:  product, category : category)
            return cell
            
        }else{
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as? CategoryCollectionViewCell else {
                return UICollectionViewCell()
            }
            guard indexPath.row < self.viewModel?.categorys.count ?? 0 ,let category =  self.viewModel?.categorys[indexPath.row] else {
                return UICollectionViewCell()
            }
            if self.viewModel?.selectedCategoryIndex == indexPath {
                cell.selectedItem()
            }else{
                cell.deselectItem()
            }
            cell.configure(with: category)
            
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // did select product
        if collectionView == productsCollectionView{
            
            let product =  viewModel?.getProducts(at: indexPath.row) ?? Product.init()
            let category = viewModel?.getCategorys(at: Int((product.category_id ?? 1)) - 1) ?? CategoryProduct.init()
            
            self.viewModel?.showProductDetails(product: product, category: category)
            
            
            // did select Category
        }else  if collectionView == categoryCollectionView{
            
            
            let category = self.viewModel?.getCategorys(at:indexPath.row)
            guard let cell = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell else { return}
            
            if self.viewModel?.selectedCategoryIndex == indexPath {
                cell.deselectItem()
                self.viewModel?.products = self.viewModel?.originalProducts ?? []
                self.viewModel?.selectedCategoryIndex = nil
            } else {
                cell.selectedItem()
                self.viewModel?.products = self.viewModel?.originalProducts.filter { $0.category_id == category?.id } ?? []
                self.viewModel?.selectedCategoryIndex = indexPath
            }
            
            self.productsCollectionView.reloadData()
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if collectionView == categoryCollectionView{
            guard let cell = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell else {return}
            cell.deselectItem()
            self.viewModel?.products = self.viewModel?.originalProducts ?? []
            self.productsCollectionView.reloadData()
        }
    }
    
}


extension UINavigationController {
    
    func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    
}
