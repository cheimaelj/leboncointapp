//
//  ProductsListViewModel.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import Foundation

class ProductsListViewModel {
    
    var coordinator: ProductsListCoordinator?
    var products = [Product]()
    var originalProducts = [Product]()
    var categorys = [CategoryProduct]()
    var refreshViewProducts = {}
    var refreshViewCategorys = {}
    var showError: (String)-> Void = { _ in }
    var showLoader: (Bool)-> Void = { _ in }
    var selectedCategoryIndex: IndexPath?
    
    private var announcementService = ProductService()
    
    private var isConnectedToNetwork: Bool{
        Current.networkStatus.isConnected
    }
    
    // MARK: - public functions
    
    func getProductsAndCategorys(){
        guard isConnectedToNetwork else {
            showError(NetworkError.noInternetConnection.localizedDescription)
            return
        }
        showLoader(true)
        getCategorys()
        getProducts()
    }
    
    func getProducts(at index: Int) -> Product? {
        guard index >= 0 && index < products.count else {
            return nil
        }
        return products[index]
    }
    
    func getCategorys(at index: Int) -> CategoryProduct? {
        guard index >= 0 && index < categorys.count else {
            return nil
        }
        return categorys[index]
    }
    
    func getProducts() {
        guard isConnectedToNetwork else {
            showError(NetworkError.noInternetConnection.localizedDescription)
            return
        }
        announcementService.fetchProducts() {[weak self] result in
            self?.showLoader(false)
            switch result {
            case .success(let products):
                self?.refeshProducts(with: products)
            case .failure(let error):
                self?.showError((error as? NetworkError )?.localizedDescription ?? "")
            }
        }
    }
    
    func getCategorys(){
        guard isConnectedToNetwork else {
            showError(NetworkError.noInternetConnection.localizedDescription)
            return
        }
        announcementService.fetchCategorys() {[weak self] result in
            self?.showLoader(false)
            switch result {
            case .success(let categorysResult):
                self?.refeshCategorys(with: categorysResult)
            case .failure(let error):
                self?.showError((error as? NetworkError ).debugDescription)
            }
        }
        
    }
    
    public func refeshProducts(with productResult: [Product] = []) {
        
        let urgentProducts = (productResult.filter { $0.is_urgent == true }).sorted(by: {$0.creation_date ?? "" < $1.creation_date ?? ""})
        
        let noUrgentProducts = (productResult.filter { $0.is_urgent == false }).sorted(by: {$0.creation_date ?? "" < $1.creation_date ?? ""})

        let sortedProducts = urgentProducts + noUrgentProducts
        
        products = sortedProducts
        originalProducts = sortedProducts
        refreshViewProducts()
    }
    
    public func refeshCategorys(with categoryResult: [CategoryProduct] = []) {
        categorys = categoryResult
        refreshViewCategorys()
    }
    
    func showProductDetails(product: Product, category: CategoryProduct) {
        coordinator?.showProductDetailsScreen(product: product, category: category)
    }
    
    
}

