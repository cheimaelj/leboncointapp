# LeboncointApp


Cette application permet aux utilisateurs de consulter une liste d'annonces comprenant des images, des catégories, des titres et des prix pour chaque annonce. Chaque annonce est également accompagnée d'un indicateur pour indiquer si elle est urgente.

L'application utilise l'architecture MVVM (Model-View-ViewModel) pour séparer les responsabilités de chaque composant et faciliter la maintenance du code. Les ViewModels sont gérés par des Coordinators, qui sont des objets qui gèrent la navigation entre les différentes vues de l'application. 

En outre, l'application prend en charge le mode sombre et le mode clair, ce qui permet aux utilisateurs de choisir l'apparence qui leur convient le mieux. L'application est également compatible avec iOS 14+.

# Fonctionnalités

    Consultation d'une liste d'annonces avec images, catégories, titres et prix.
    Indicateur pour les annonces urgentes.
    Filtre pour afficher les annonces par catégorie.
    Tri par date avec les annonces urgentes en tête de liste.
    Mode sombre et mode clair.
    Test unitaire pour garantir la qualité du code.

# Utilisation

    Clonez le projet depuis GitHub.
    Ouvrez le projet dans Xcode.
    Compilez et exécutez l'application sur un émulateur ou un appareil iOS.
    Consultez la liste des annonces et utilisez les filtres et les options de tri pour affiner vos résultats.

